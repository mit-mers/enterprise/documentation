(in-package #:mit-mers-documentation-site)

(defun details-for (name)
  (let* ((out ())
         (name (string-downcase (string name)))
         (all-versions (uiop:subdirectories (asdf:system-relative-pathname
                                             :mit-mers-documentation-site
                                             (format nil "projects/~A/" name)))))
    (setf all-versions (mapcar (lambda (x)
                                 (car (last (pathname-directory x))))
                               all-versions))
    (setf all-versions (remove "latest" all-versions :test #'equal))
    (setf all-versions (sort all-versions #'uiop:version<
                             ;; Strip the leading v
                             :key (lambda (x) (subseq x 1))))
    (unless (null all-versions)
      (push (format nil "- [latest](projects/~A/latest/)" name) out))
    (loop :for v :in all-versions
          :do
             (push (format nil "- [~A](projects/~A/~A/)" v name v) out))
    (nreverse out)))
