(require "asdf")

(asdf:load-system :mit-mers-documentation-site)

(mit-mers-documentation-site::generate-docs)
