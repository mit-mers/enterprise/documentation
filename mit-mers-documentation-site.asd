;;; -*- mode: lisp -*-

(asdf:defsystem #:mit-mers-documentation-site
  :version (:read-file-form "version.lisp-expr")
  :author "Model-based Embedded and Robotic Systems Group, MIT"
  :description "Documentation site for MIT MERS code"
  :license "BSD-2"
  :depends-on ("40ants-doc-full" "40ants-doc")
  :pathname "src/"
  :components ((:file "package")
               (:file "utils" :depends-on ("package"))
               (:file "site" :depends-on ("package" "utils"))))
